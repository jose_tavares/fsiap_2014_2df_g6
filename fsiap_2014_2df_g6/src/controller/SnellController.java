/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import model.Meio;
import model.Snell;
import persistence.MeioPersistence;
import persistence.SnellPersistence;

/**
 *
 * FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class SnellController {

    private final Snell snell;

    public SnellController() {
        snell = new Snell();
    }

    public SnellController(Meio meio1, Meio meio2) {
        snell = new Snell(meio1, meio2);
    }

    public SnellController(Meio meio1, Meio meio2, double incidencia) {
        snell = new Snell(meio1, meio2, incidencia);
    }

    public SnellController(Snell snell) {
        this.snell = snell;
    }

    public Snell getSnell() {
        return snell;
    }

    public void comportamentoAngularByMeio(Meio meioIncidencia, Meio meioRefratado, int incrementar)
            throws IOException {
        SnellPersistence results = new SnellPersistence();

        String meioResult = meioIncidencia + "\n" + meioRefratado;
        System.out.println(meioResult);

        String titulo = "Incidencia\tReflexao\tRefraccao";
        System.out.println(titulo);

        snell.setMeio1(meioIncidencia);
        snell.setMeio2(meioRefratado);
        for (int i = 0; i <= 90; i += incrementar) {
            snell.setAnguloIncidencia(i);
            snell.anguloRefratado();

            String refratadoResult;
            if (!Double.isNaN(snell.getAnguloRefratado())) {
                refratadoResult = String.format("%.2fº\t\t%.2fº\t\t%.2fº", snell.getAnguloIncidencia(),
                        snell.getAnguloRefletido(), snell.getAnguloRefratado());
                System.out.println(refratadoResult);
            } else {
                refratadoResult = String.format("%.2fº\t\t%.2fº\t\t-",
                        snell.getAnguloIncidencia(), snell.getAnguloRefletido());
                System.out.println(refratadoResult);
            }
        }
        snell.anguloCritico();

        String meioCriticoResult;
        if (!Double.isNaN(snell.getAnguloIncidencia())) {
            meioCriticoResult = "\nAlgulo critico entre o meio de incidencia " + meioIncidencia.getNome()
                    + " com o meio " + meioRefratado.getNome();
            System.out.println(meioCriticoResult);

            String incidenciaResult = String.format("%.2fº\t\t%.2fº", snell.getAnguloRefletido(),
                    snell.getAnguloRefratado());
            System.out.println(incidenciaResult);
        } else {
            meioCriticoResult = "\nNao existe angulo critico entre o meio de incidencia "
                    + meioIncidencia.getNome() + " com o meio de refracao " + meioRefratado.getNome();
            System.out.println(meioCriticoResult);
        }

        results.add(snell);
        results.saveToBinary("calculadora");

    }

    /**
     * dado um meio, e um angulo de incidencia calcula varios angulos de
     * refração variando para isso os meios
     *
     * @param meioIncidencia
     * @param incidencia
     */
    public void comportamentoAngularByIncidencia(Meio meioIncidencia, double incidencia) {
        try {
            SnellPersistence results = new SnellPersistence();

            String meio1Result = "Meio de Incidencia: " + meioIncidencia.getNome()
                    + String.format(" -> %.2fº", incidencia) + '\n';
            System.out.println(meio1Result);

            //define o maio de incidencia para snell(objecto responsavel por calculos)
            snell.setMeio1(meioIncidencia);

            //define o angulo de incidencia para snell
            snell.setAnguloIncidencia(incidencia);

            //carregar lista meios (ficheiro de meios) e ordena-los
            MeioPersistence list_meios = MeioPersistence.getInstance();

            //ciclo que corre todos os meios da lista
            String meio2Result = "Meios de refracao:";
            System.out.println(meio2Result);

            list_meios.stream().map((Meio meio) -> {
                //calcula angulo de refração
                snell.setMeio2(meio);
                return meio;
            }).map((meio) -> {
                snell.anguloRefratado();
                return meio;
            }).forEach((Meio meio) -> {
                double anguloRefratado = snell.getAnguloRefratado();
                //output
                String refracaoResult = meio.getNome() + "\n" + String.format("%.2fº<br>", anguloRefratado);
                System.out.println(refracaoResult);
                results.add(snell);
            });
            results.saveToBinary("estudo");
        } catch (IOException ex) {
            System.out.println("Ficheiro não criado");
        }
    }

    /**
     * dado o meio refração, e um angulo de incidencia calcula varios angulos de
     * incidencia variando para isso os meios de incidencia
     *
     * @param meioRefracao
     * @param incidencia
     */
    public void comportamentoAngularByRefracao(Meio meioRefracao, double incidencia) {
        try {
            SnellPersistence results = new SnellPersistence();

            String meio2Result = "Meio de refracao: " + meioRefracao.getNome() + '\n';
            System.out.println(meio2Result);

            //define o maio de refração para snell(objecto responsavel por calculos)
            snell.setMeio2(meioRefracao);

            //define o angulo de incidencia para snell
            snell.setAnguloIncidencia(incidencia);

            //carregar lista meios (ficheiro de meios)
            MeioPersistence list_meios = MeioPersistence.getInstance();

            //ciclo que corre todos os meios da lista
            String meio1Result = "Meios de incidencia:";
            System.out.println(meio1Result);

            list_meios.stream().map((Meio meio) -> {
                //calcula angulo de incidencia
                snell.setMeio1(meio);
                return meio;
            }).map((meio) -> {
                snell.anguloRefratado();
                return meio;
            }).forEach((Meio meio) -> {
                double anguloRefletido = snell.getAnguloRefratado();

                //output
                String incidenciaResult;
                if (!Double.isNaN(anguloRefletido)) {
                    incidenciaResult = meio.getNome() + String.format("\n%.2fº\t\t", incidencia)
                            + String.format("%.2fº\t\t", anguloRefletido);
                    System.out.println(incidenciaResult);
                } else {
                    incidenciaResult = meio.getNome() + String.format("\n%.2fº\t\t", incidencia) + "-";
                    System.out.println(incidenciaResult);
                }
                results.add(snell);
            });

            results.saveToBinary("estudo");
        } catch (IOException ex) {
            System.out.println("Ficheiro não criado");
        }
    }
}
