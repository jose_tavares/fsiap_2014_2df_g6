/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap_2014_2df_g6;

import controller.SnellController;
import java.util.Locale;
import java.util.Scanner;
import model.Meio;
import persistence.MeioPersistence;
import ui.MainFrame;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*Scanner ler = new Scanner(System.in);

        SnellController snellController = new SnellController();
        MeioPersistence meios = MeioPersistence.getInstance();
        Meio meioIncidencia, meioRefracao;
        meios.load_ficheiro_to_list();
        meios.print();

        System.out.println("\n1 - Comportamento Angular por dois Meios");
        System.out.println("2 - Comportamento Angular por Meio de Incidencia");
        System.out.println("3 - Comportamento Angular por Meio de Refracao");
        System.out.println("Opcao:");

        int opcao = ler.nextInt();

        switch (opcao) {
            case 1:
                System.out.println("Meio de incidencia:\n");
                meioIncidencia = meios.findMeioByNome(ler.next());
                System.out.println("Meio de refracao:\n");
                meioRefracao = meios.findMeioByNome(ler.next());
                System.out.println("Incremento:\n");
                int incremento = ler.nextInt();

                if (meioIncidencia != null && meioRefracao != null) {
                    System.out.println("Angulo de incidencia:\n");
                    snellController.comportamentoAngularByMeio(meioIncidencia, meioRefracao, incremento);
                }
                break;
            case 2:
                System.out.println("Meio de incidencia:\n");
                meioIncidencia = meios.findMeioByNome(ler.next());
                if (meioIncidencia != null) {
                    System.out.println("Angulo de incidencia:\n");
                    snellController.comportamentoAngularByIncidencia(meioIncidencia, ler.nextInt());
                }
                break;
            case 3:
                System.out.println("Meio de refracao:\n");
                meioRefracao = meios.findMeioByNome(ler.next());
                if (meioRefracao != null) {
                    System.out.println("Angulo de incidencia:\n");
                    snellController.comportamentoAngularByRefracao(meioRefracao, ler.nextInt());
                }
                break;
            default:
                System.out.println("Sair");
                break;
        }*/
                
        MainFrame mainFrame = MainFrame.getInstance();
        mainFrame.setVisible(true);
        mainFrame.setLocationRelativeTo(null);
        
        Locale locale = Locale.getDefault();
        System.out.println(locale.getDisplayLanguage());
    }
}
