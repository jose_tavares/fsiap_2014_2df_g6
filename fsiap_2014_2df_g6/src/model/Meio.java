/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class Meio implements Serializable{

    //1
    private String nome;
    private double velocidade;
    private double indice;

    //2
    public Meio(String nome, double velocidade, double indice) {
        this.nome = nome == null || nome.isEmpty() ? "Meio desconhecido" : nome;
        this.velocidade = velocidade <= 3 ? velocidade : 0;
        this.indice = indice >= 1 ? indice : 0;
    }

    public Meio(String nome, double indice) {
        this(nome, 0, indice);
    }

    public Meio() {
        this("Meio desconhecido", 0, 0);
    }

    public Meio(double indice) {
        this("Meio desconhecido", 0, indice);
    }

    public Meio(Meio meio) {
        this(meio.nome, meio.velocidade, meio.indice);
    }

    //3  Metodos acesso e modificadores
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome == null || nome.isEmpty() ? "Meio desconhecido" : nome;
    }

    public double getVelocidade() {
        if (velocidade == 0 && indice >= 1) {
            velocidadeByIndice();
        }
        return velocidade;
    }

    public void setVelocidade(double velocidade) {
        this.velocidade = velocidade <= 3 ? velocidade : 0;
    }

    public double getIndice() {
        if (velocidade != 0 && indice == 0) {
            indiceByVelocidade();
        }
        return indice;
    }

    public void setIndice(double indice) {
        this.indice = indice >= 1 ? indice : 0;
    }

    //4  Calculos
    private void velocidadeByIndice() {
        velocidade = 3 / indice;
    }

    private void indiceByVelocidade() {
        indice = 3 / velocidade;
    }

    //5
    @Override
    public String toString() {
        String string = this.nome;

        if (velocidade == 0 && indice >= 1) {
            velocidadeByIndice();
        } else if (velocidade != 0 && indice == 0) {
            indiceByVelocidade();
        } else if (velocidade == 0 && indice == 0) {
            return string;
        }

        return string + " » " + String.format("%.2f", indice);
    }
}
