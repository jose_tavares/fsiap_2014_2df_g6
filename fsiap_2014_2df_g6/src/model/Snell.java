/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Locale;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class Snell implements Serializable {

    //1
    private Meio meio1; //meio incidencia
    private Meio meio2; //meio refratado
    private double incidencia;
    private double refletido;
    private double refratado;

    //2
    public Snell(Meio meio1, Meio meio2, double incidencia) {
        this.meio1 = meio1;
        this.meio2 = meio2;
        this.incidencia = incidencia >= 0 || incidencia <= 90 ? incidencia : 0;
    }

    public Snell() {
        this(new Meio(), new Meio(), 0);
    }

    public Snell(Meio meio1, Meio meio2) {
        this(meio1, meio2, 0);
    }

    public Snell(Snell snell) {
        this(snell.meio1, snell.meio2, snell.incidencia);
    }

    //3 Metodos acesso e modificadores
    public Meio getMeio1() {
        return meio1;
    }

    public void setMeio1(Meio meio1) {
        this.meio1 = meio1;
    }

    public Meio getMeio2() {
        return meio2;
    }

    public void setMeio2(Meio meio2) {
        this.meio2 = meio2;
    }

    public double getAnguloIncidencia() {
        return incidencia;
    }

    public void setAnguloIncidencia(double incidencia) {
        this.incidencia = incidencia >= 0 || incidencia <= 90 ? incidencia : 0;
    }

    public double getAnguloRefletido() {
        anguloRefletido();
        return refletido;
    }

    public double getAnguloRefratado() {
        return refratado;
    }

    public void setAnguloRefratado(double refratado) {
        this.refratado = refratado >= 0 || refratado <= 90 ? refratado : 0;
    }

    //4 Calculos
    private void anguloRefletido() {
        refletido = incidencia;
    }

    /**
     * Se o resultado for NaN significa que nao existe refraccao
     */
    public void anguloRefratado() {
        refratado = Math.toDegrees(Math.asin(meio1.getIndice()
                * Math.sin(Math.toRadians(incidencia)) / meio2.getIndice()));
    }

    /**
     * Se o resultado for NaN significa que nao existe reflexao total
     */
    public void anguloCritico() {
        refratado = 90;
        incidencia = Math.toDegrees(Math.asin(meio2.getIndice() / meio1.getIndice()));
        anguloRefletido();
    }

    //5
    @Override
    public String toString() {
        anguloRefletido();
        String string = "Meio 1: " + meio1 + "\nMeio 2: " + meio2
                + "\n\nAngulos:\nincidencia - " + String.format("%.2f", incidencia)
                + "º refletido - " + String.format("%.2f", refletido) + 'º';
        if (!Double.isNaN(refratado)) {
            return string + " refratado - " + String.format("%.2f", refratado) + 'º';
        }
        return string + " refratado - nao tem";
    }

    public String toHtml() {
        anguloRefletido();
        String string;
        Locale locale = Locale.getDefault();
        if (locale.getDisplayLanguage().equals("English")) {
            string = "Medium 1: " + meio1 + "<br>Medium 2: " + meio2
                    + "<br><br>Angles:<br>Incidence - " + String.format("%.2f", incidencia)
                    + "º Reflective - " + String.format("%.2f", refletido) + 'º';
            if (!Double.isNaN(refratado)) {
                return string + " Refracted - " + String.format("%.2f", refratado) + 'º';
            }
            string += " Refracted - not existent";
        } else {
            string = "Meio 1: " + meio1 + "<br>Meio 2: " + meio2
                    + "<br><br>Angulos:<br>incidente - " + String.format("%.2f", incidencia)
                    + "º refletido - " + String.format("%.2f", refletido) + 'º';
            if (!Double.isNaN(refratado)) {
                return string + " refratado - " + String.format("%.2f", refratado) + 'º';
            }
            string += " refratado - nao tem";
        }
        return string;
    }
}
