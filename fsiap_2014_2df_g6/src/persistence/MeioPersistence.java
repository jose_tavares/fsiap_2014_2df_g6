/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Scanner;
import model.Meio;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class MeioPersistence extends ArrayList<Meio> {

    private static final MeioPersistence instance = new MeioPersistence();

    private MeioPersistence() {

    }

    @Override
    public boolean add(Meio item) {
        if (isEmpty() || !contains(findMeioByNome(item.getNome()))) {
            super.add(item);
            Collections.sort(this, (Meio t, Meio t1) -> Double.compare(t.getIndice(), t1.getIndice()));
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof Meio) {
            Meio item = (Meio) o;
            if (!isEmpty() && contains(findMeioByNome(item.getNome()))) {
                super.remove(item);
                return true;
            }
        }
        return false;
    }

    public Meio findMeioByNome(String nome) {
        if (!isEmpty()) {
            for (Meio meios : this) {
                if (nome.equals(meios.getNome())) {
                    return meios;
                }
            }
        }
        return null;
    }

    public Meio findMeioByIndice(double indice) {
        if (!isEmpty()) {
            for (Meio meios : this) {
                if (indice == meios.getIndice()) {
                    return meios;
                }
            }
        }
        return null;
    }

    public void print() {
        this.stream().forEach((Meio meios) -> {
            System.out.println(meios);
        });
    }

    public static MeioPersistence getInstance() {
        return instance;
    }

    //carrega conteudo de ficheiro de meios
    //e seus indices para uma lista de meios
    public void load_ficheiro_to_list() throws FileNotFoundException {
        //corre ficheiro
        Scanner input;
        Locale locale = Locale.getDefault();
        //inicializar classe de input
        if (locale.getDisplayLanguage().equals("English")) {
            input = new Scanner(new File("src\\fsiap_2014_2df_g6\\en_GB.txt"));
        } else {
            input = new Scanner(new File("src\\fsiap_2014_2df_g6\\pt_PT.txt"));
        }

        //corrre ficheiro
        while (input.hasNextLine()) {
            //ler linha
            String linha = input.nextLine();
            if (linha.length() > 0) {
                String[] campos = linha.split(",");
                //criar meio e adicionar a lista
                add(new Meio(campos[0], Double.parseDouble(campos[1])));
            }
        }
        input.close();
    }
}
