/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import model.Meio;
import model.Snell;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class SnellPersistence extends ArrayList<Snell> {

    private final String date = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss").format(new Date());
    private final Locale locale = Locale.getDefault();

    public SnellPersistence() {
    }

    public void saveToBinary(String path) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("results\\binary\\" + path + "\\" + date + ".bin"))) {
            out.writeObject(this);
            out.close();
        }
    }

    public void saveToBinaryByMeio1(Snell snell, String path) throws IOException {
        MeioPersistence.getInstance().stream().map((meio2) -> {
            snell.setMeio2(meio2);
            return meio2;
        }).map((Meio _item) -> {
            snell.anguloRefratado();
            return _item;
        }).forEach((Meio _item) -> {
            this.add(snell);
        });

        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("results\\binary\\" + path + "\\" + date + ".bin"))) {
            out.writeObject(this);
            out.close();
        }
    }

    public void saveToBinaryByMeio2(Snell snell, String path) throws IOException {
        MeioPersistence.getInstance().stream().map((meio1) -> {
            snell.setMeio1(meio1);
            return meio1;
        }).map((Meio _item) -> {
            snell.anguloRefratado();
            return _item;
        }).forEach((Meio _item) -> {
            this.add(snell);
        });

        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("results\\binary\\" + path + "\\" + date + ".bin"))) {
            out.writeObject(this);
            out.close();
        }
    }

    public void saveToHtml(Snell snell, String path) throws IOException {
        String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n\"http://www.w3.org/TR/html4/strict.dtd\">\n"
                + "<html>\n"
                + "\t<head>\n"
                + "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                + "\t\t<title>" + date + "</title>\n"
                + "\t</head>\n"
                + "\t<body>\n";
        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<h1>Result of Calculator</h1>\n";
        } else {
            html += "\t\t<h1>Resultado de Calculadora</h1>\n";
        }

        html += snell.toHtml() + '\n';

        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<p>Working group: G06 <br>\n";
        } else {
            html += "\t\t<p>Grupo de trabalho: G06 <br>\n";
        }

        html += "\t\tJosé Tavares (1111228), Paulo Costa (1101413), Joás V. Pereira (1110166)</p>\n"
                + "\t</body>"
                + "</html>";

        File file = new File("results\\html\\" + path + "\\" + date + ".html");
        try (Writer writeHtml = new BufferedWriter(new FileWriter(file))) {
            writeHtml.write(html);
            writeHtml.close();
        }

    }

    public void saveToCSV(Snell snell, String path) throws IOException {
        String csv;
        if (locale.getDisplayLanguage().equals("English")) {
            csv = "Medium 1;Medium 2;Angle of Incidence;Reflective Angle;Refracted Angle\n"
                    + snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                    + ')' + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                    + ')' + ';' + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                    + String.format("%.2f", snell.getAnguloRefletido()) + ';';

            if (!Double.isNaN(snell.getAnguloRefratado())) {
                csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
            } else {
                csv += "not existent\n";
            }
        } else {
            csv = "Meio 1;Meio 2;Angulo Incidente;Angulo Refletido;Angulo Refratado\n"
                    + snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                    + ')' + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                    + ')' + ';' + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                    + String.format("%.2f", snell.getAnguloRefletido()) + ';';

            if (!Double.isNaN(snell.getAnguloRefratado())) {
                csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
            } else {
                csv += "não tem\n";
            }
        }

        File file = new File("results\\csv\\" + path + "\\" + date + ".csv");
        try (Writer writeCSV = new BufferedWriter(new FileWriter(file))) {
            writeCSV.write(csv);
            writeCSV.close();
        }
    }

    public void saveToCSVByMeio1(Snell snell, String path) throws IOException {
        String csv;
        if (locale.getDisplayLanguage().equals("English")) {
            csv = "Medium 1;N1;Medium 2;N2;Angle of Incidence;Reflective Angle;Refracted Angle\n";
            for (Meio meio2 : MeioPersistence.getInstance()) {
                snell.setMeio2(meio2);
                snell.anguloRefratado();

                double anguloRefratado = snell.getAnguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';

                if (!Double.isNaN(anguloRefratado)) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "not existent\n";
                }
            }

        } else {
            csv = "Meio 1;N1;Meio 2;N2;Angulo Incidente;Angulo Refletido;Angulo Refratado\n";
            for (Meio meio2 : MeioPersistence.getInstance()) {
                snell.setMeio2(meio2);
                snell.anguloRefratado();

                double anguloRefratado = snell.getAnguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';

                if (!Double.isNaN(anguloRefratado)) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "não tem\n";
                }
            }

        }

        File file = new File("results\\csv\\" + path + "\\" + date + ".csv");
        try (Writer writeCSV = new BufferedWriter(new FileWriter(file))) {
            writeCSV.write(csv);
            writeCSV.close();
        }
    }

    public void saveToCSVByMeio2(Snell snell, String path) throws IOException {
        String csv;
        if (locale.getDisplayLanguage().equals("English")) {
            csv = "Medium 1;N1;Medium 2;N2;Angle of Incidence;Reflective Angle;Refracted Angle\n";
            for (Meio meio1 : MeioPersistence.getInstance()) {
                snell.setMeio1(meio1);
                snell.anguloRefratado();

                double anguloRefratado = snell.getAnguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';

                if (!Double.isNaN(anguloRefratado)) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "not existent\n";
                }
            }
        } else {
            csv = "Meio 1;N1;Meio 2;N2;Angulo Incidente;Angulo Refletido;Angulo Refratado\n";
            for (Meio meio1 : MeioPersistence.getInstance()) {
                snell.setMeio1(meio1);
                snell.anguloRefratado();

                double anguloRefratado = snell.getAnguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';

                if (!Double.isNaN(anguloRefratado)) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "não tem\n";
                }
            }
        }

        File file = new File("results\\csv\\" + path + "\\" + date + ".csv");
        try (Writer writeCSV = new BufferedWriter(new FileWriter(file))) {
            writeCSV.write(csv);
            writeCSV.close();
        }
    }

    public void saveToHtmlByMeio1(Snell snell, String path) throws IOException {
        String meio1Result, meio2Result, refracaoResult = "";
        if (locale.getDisplayLanguage().equals("English")) {
            meio1Result = "Medium 1: " + snell.getMeio1().getNome()
                    + String.format(" (%.2f)", snell.getMeio1().getIndice())
                    + String.format("<br>Angle of Incidence: %.2fº", snell.getAnguloIncidencia()) + "<br>";

            //ciclo que corre todos os meios da lista
            meio2Result = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Medium 2</th>\n"
                    + "\t\t<th>Refracted Angle</th>\n"
                    + "\t</tr>\n";

            for (Meio meio : MeioPersistence.getInstance()) {
                snell.setMeio2(meio);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>"
                            + String.format("\t\t<td>%.2fº</td>\n\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t\t<td>" + meio.getNome() + "</td>" + "\t\t<td>not existent</td>\n\t</tr>";
                }
            }
        } else {
            meio1Result = "Meio 1: " + snell.getMeio1().getNome()
                    + String.format(" (%.2f)", snell.getMeio1().getIndice())
                    + String.format("<br>Ângulo de incidência: %.2fº", snell.getAnguloIncidencia()) + "<br>";

            //ciclo que corre todos os meios da lista
            meio2Result = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Meio 2</th>\n"
                    + "\t\t<th>Ângulo de refração</th>\n"
                    + "\t</tr>\n";

            for (Meio meio : MeioPersistence.getInstance()) {
                snell.setMeio2(meio);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>"
                            + String.format("\t\t<td>%.2fº</td>\n\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t\t<td>" + meio.getNome() + "</td>" + "\t\t<td>não tem</td>\n\t</tr>";
                }
            }
        }

        String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n\"http://www.w3.org/TR/html4/strict.dtd\">\n"
                + "<html>\n"
                + "\t<head>\n"
                + "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                + "\t\t<title>" + date + "</title>\n"
                + "\t</head>\n"
                + "\t<body>\n";
        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<h1>Result of Study</h1>\n";
        } else {
            html += "\t\t<h1>Resultado de Estudo</h1>\n";
        }
        html += meio1Result + meio2Result + refracaoResult + "</table>\n";

        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<p>Working group: G06 <br>\n";
        } else {
            html += "\t\t<p>Grupo de trabalho: G06 <br>\n";
        }

        html += "\t\tJosé Tavares (1111228), Paulo Costa (1101413), Joás V. Pereira (1110166)</p>\n"
                + "\t</body>"
                + "</html>";

        File file = new File("results\\html\\" + path + "\\" + date + ".html");
        try (Writer writeHtml = new BufferedWriter(new FileWriter(file))) {
            writeHtml.write(html);
            writeHtml.close();
        }
    }

    public void saveToHtmlByMeio2(Snell snell, String path) throws IOException {
        String meio1Result, meio2Result, refracaoResult = "";

        if (locale.getDisplayLanguage().equals("English")) {
            meio2Result = "Medium 2: " + snell.getMeio2().getNome()
                    + String.format(" (%.2f)", snell.getMeio2().getIndice())
                    + String.format("<br>Angle of Incidence: %.2fº", snell.getAnguloIncidencia()) + "<br>";

            //ciclo que corre todos os meios da lista
            meio1Result = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Medium 1</th>\n"
                    + "\t\t<th>Angle of refraction of Medium 2</th>\n"
                    + "\t</tr>\n";

            for (Meio meio : MeioPersistence.getInstance()) {
                snell.setMeio1(meio);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>"
                            + String.format("\t\t<td>%.2fº</td>\n\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>" + "\t\t<td>not existent</td>\n\t</tr>";
                }
            }
        } else {
            meio2Result = "Meio 2: " + snell.getMeio2().getNome()
                    + String.format(" (%.2f)", snell.getMeio2().getIndice())
                    + String.format("<br>Ângulo de incidência: %.2fº", snell.getAnguloIncidencia()) + "<br>";

            //ciclo que corre todos os meios da lista
            meio1Result = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Meio 1</th>\n"
                    + "\t\t<th>Ângulo de refração do Meio 2</th>\n"
                    + "\t</tr>\n";

            for (Meio meio : MeioPersistence.getInstance()) {
                snell.setMeio1(meio);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>"
                            + String.format("\t\t<td>%.2fº</td>\n\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t<tr>"
                            + "\t\t<td>" + meio.getNome()
                            + String.format(" (%.2f)", meio.getIndice()) + "</td>" + "\t\t<td>não tem</td>\n\t</tr>";
                }
            }
        }

        String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n\"http://www.w3.org/TR/html4/strict.dtd\">\n"
                + "<html>\n"
                + "\t<head>\n"
                + "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                + "\t\t<title>" + date + "</title>\n"
                + "\t</head>\n"
                + "\t<body>\n";
        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<h1>Result of Study</h1>\n";
        } else {
            html += "\t\t<h1>Resultado de Estudo</h1>\n";
        }
        html += meio1Result + meio2Result + refracaoResult + "</table>\n";

        if (locale.getDisplayLanguage().equals("English")) {
            html += "\t\t<p>Working group: G06 <br>\n";
        } else {
            html += "\t\t<p>Grupo de trabalho: G06 <br>\n";
        }

        html += "\t\tJosé Tavares (1111228), Paulo Costa (1101413), Joás V. Pereira (1110166)</p>\n"
                + "\t</body>"
                + "</html>";

        File file = new File("results\\html\\" + path + "\\" + date + ".html");
        try (Writer writeHtml = new BufferedWriter(new FileWriter(file))) {
            writeHtml.write(html);
            writeHtml.close();
        }
    }

    public void saveToCSVInc(Snell snell, String path) throws IOException {
        String csv;
        if (locale.getDisplayLanguage().equals("English")) {
            csv = "Medium 1;N1;Medium 2;N2;Angle of Incidence;Reflective Angle;Refracted Angle\n";

            for (int i = 5; i <= 85; i += 5) {
                snell.setAnguloIncidencia(i);
                snell.anguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';

                if (!Double.isNaN(snell.getAnguloRefratado())) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "not existent\n";
                }
            }

        } else {
            csv = "Meio 1;N1;Meio 2;N2;Angulo Incidente;Angulo Refletido;Angulo Refratado\n";

            for (int i = 5; i <= 85; i += 5) {
                snell.setAnguloIncidencia(i);
                snell.anguloRefratado();

                csv += snell.getMeio1().getNome() + '(' + snell.getMeio1().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio1().getIndice())
                        + ';' + snell.getMeio2().getNome() + '(' + snell.getMeio2().getIndice()
                        + ')' + ';' + String.format("%.2f", snell.getMeio2().getIndice()) + ';'
                        + String.format("%.2f", snell.getAnguloIncidencia()) + ';'
                        + String.format("%.2f", snell.getAnguloRefletido()) + ';';
                if (!Double.isNaN(snell.getAnguloRefratado())) {
                    csv += String.format("%.2f", snell.getAnguloRefratado()) + '\n';
                } else {
                    csv += "não tem\n";
                }
            }
        }

        File file = new File("results\\csv\\" + path + "\\" + date + ".csv");
        try (Writer writeCSV = new BufferedWriter(new FileWriter(file))) {
            writeCSV.write(csv);
            writeCSV.close();
        }
    }

    public void saveToHtmlInc(Snell snell, String path) throws IOException {
        String meio1Result, meio2Result, refracaoResult = "";

        if (locale.getDisplayLanguage().equals("English")) {
            meio1Result = "Medium 1: " + snell.getMeio1().getNome()
                    + String.format(" (%.2f)", snell.getMeio1().getIndice()) + "<br>";

            meio2Result = "Medium 2: " + snell.getMeio2().getNome()
                    + String.format(" (%.2f)", snell.getMeio2().getIndice()) + "<br>";

            //ciclo que corre todos os meios da lista
            refracaoResult = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Angle of Incidence</th>\n"
                    + "\t\t<th>Refracted Angle</th>\n"
                    + "\t</tr>\n";

            for (int i = 5; i <= 85; i += 5) {
                snell.setAnguloIncidencia(i);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + String.format("\t\t<td>%.2fº</td>", snell.getAnguloIncidencia())
                            + String.format("\t\t<td>%.2fº</td>\n\t\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t<tr>"
                            + String.format("\t\t<td>%.2fº</td>", snell.getAnguloIncidencia())
                            + "\t\t<td>not existent</td>\n\t\t</tr>";
                }
            }
        } else {
            meio1Result = "Meio 1: " + snell.getMeio1().getNome()
                    + String.format(" (%.2f)", snell.getMeio1().getIndice()) + "<br>";

            meio2Result = "Meio 2: " + snell.getMeio2().getNome()
                    + String.format(" (%.2f)", snell.getMeio2().getIndice()) + "<br>";

            //ciclo que corre todos os meios da lista
            refracaoResult = "<table border=\"1\" style=\"width:50%\">"
                    + "\t<tr>\n"
                    + "\t\t<th>Ângulo Incidente</th>\n"
                    + "\t\t<th>Ângulo Refratado</th>\n"
                    + "\t</tr>\n";

            for (int i = 5; i <= 85; i += 5) {
                snell.setAnguloIncidencia(i);
                snell.anguloRefratado();
                double anguloRefratado = snell.getAnguloRefratado();

                if (!Double.isNaN(anguloRefratado)) {
                    refracaoResult += "\t<tr>"
                            + String.format("\t\t<td>%.2fº</td>", snell.getAnguloIncidencia())
                            + String.format("\t\t<td>%.2fº</td>\n\t\t</tr>", anguloRefratado);
                } else {
                    refracaoResult += "\t<tr>"
                            + String.format("\t\t<td>%.2fº</td>", snell.getAnguloIncidencia())
                            + "\t\t<td>não existe</td>\n\t</tr>";
                }
            }
        }

        String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n\"http://www.w3.org/TR/html4/strict.dtd\">\n"
                + "<html>\n"
                + "\t<head>\n"
                + "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                + "\t\t<title>" + date + "</title>\n"
                + "\t</head>\n"
                + "\t<body>\n";

        if (locale.getDisplayLanguage()
                .equals("English")) {
            html += "\t\t<h1>Result of Calculator</h1>\n";
        } else {
            html += "\t\t<h1>Resultado de Calculadora</h1>\n";
        }
        html += meio1Result + meio2Result + refracaoResult + "</table>\n";

        if (locale.getDisplayLanguage()
                .equals("English")) {
            html += "\t\t<p>Working group: G06 <br>\n";
        } else {
            html += "\t\t<p>Grupo de trabalho: G06 <br>\n";
        }

        html += "\t\tJosé Tavares (1111228), Paulo Costa (1101413), Joás V. Pereira (1110166)</p>\n"
                + "\t</body>"
                + "</html>";

        File file = new File("results\\html\\" + path + "\\" + date + ".html");
        try (Writer writeHtml = new BufferedWriter(new FileWriter(file))) {
            writeHtml.write(html);
            writeHtml.close();
        }
    }
}
