package ui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Joás V. Pereira (1110166)
 */
public class Grafico extends JPanel {

    private final int margemX = 0, margemY = 20;
    private final double angulo1, angulo2;

    public Grafico() {
        angulo1 = Math.toRadians(45);
        angulo2 = Math.toRadians(45);
    }

    public Grafico(double anguloMeio1, double anguloMeio2) {
        angulo1 = Math.toRadians(anguloMeio1);
        angulo2 = Math.toRadians(anguloMeio2);
        setVisible(true);
    }

    private void referencial(Graphics g) {
        //definir cores
        Graphics gOrdenadas = g.create();
        gOrdenadas.setColor(Color.BLACK);
        Graphics gAbscissas = g.create();
        gAbscissas.setColor(Color.GREEN);
        //desenha eixos
        //desenha linha entre dois pontos, drawLine(pont1x,pont1y,pont2x,pont2y)
        gOrdenadas.drawLine((getWidth() / 2), margemY, (getWidth() / 2), getHeight() - margemY);
        gAbscissas.drawLine(margemX, (getHeight() / 2), getWidth() - margemX, (getHeight() / 2));

    }

    private void luzMeio1(Graphics g) {
        Graphics gLuz = g.create();
        gLuz.setColor(Color.RED);
        //CO = tan(angulo)*CA
        if (!Double.isNaN(angulo1)) {
            int ponto2y = (int) (Math.tan(Math.PI / 2 - angulo1) * getWidth());
            //desenha linha
            gLuz.drawLine(0, (getHeight() / 2) - ponto2y, (getWidth() / 2), (getHeight() / 2));
        }
    }
    
    private void luzRefletida(Graphics g) {
        Graphics gLuz = g.create();
        gLuz.setColor(Color.RED);
        //CO = tan(angulo)*CA
        if (!Double.isNaN(angulo1)) {
            int ponto2y = (int) (Math.tan(Math.PI / 2 - angulo1) * getWidth());
            //desenha linha
            gLuz.drawLine(getWidth(), (getHeight() / 2) - ponto2y, (getWidth() / 2), (getHeight() / 2));
        }
    }

    private void luzMeio2(Graphics g) {
        Graphics gLuz = g.create();
        gLuz.setColor(Color.BLUE);
        //CO = tan(angulo)*CA
        if (angulo2 != 0 && !Double.isNaN(angulo2)) {
            int ponto2y = (int) (Math.tan(Math.PI / 2 - angulo2) * getWidth());
            //desenha linha
            gLuz.drawLine(getWidth(), (getHeight() / 2) + ponto2y, (getWidth() / 2), (getHeight() / 2));
        }
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        double auxA;
        referencial(g);
        luzMeio1(g);
        luzMeio2(g);
        luzRefletida(g);
    }

}
