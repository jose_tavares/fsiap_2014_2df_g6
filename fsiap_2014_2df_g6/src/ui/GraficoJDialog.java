/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.SnellController;

/**
 *
 * @author FSIAP_2014_2DF_G6_Jose_Tavares
 */
public class GraficoJDialog extends javax.swing.JDialog {

    private final Grafico grafico;
    private final SnellController snellController;

    /**
     * Creates new form GraficoJDialog
     *
     * @param parent
     * @param modal
     * @param grafico
     * @param snellController
     */
    public GraficoJDialog(java.awt.Frame parent, boolean modal, Grafico grafico,
            SnellController snellController) {
        super(parent, modal);
        this.grafico = grafico;
        this.snellController = snellController;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelGrafico = grafico;
        jButtonVoltar = new javax.swing.JButton();
        jLabelRefracao = new javax.swing.JLabel();
        jLabelNomeMeio1 = new javax.swing.JLabel();
        jLabelIndiceMeio1 = new javax.swing.JLabel();
        jLabelIncidencia = new javax.swing.JLabel();
        jLabelNomeMeio2 = new javax.swing.JLabel();
        jLabelIndiceMeio2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("ui/Bundle"); // NOI18N
        setTitle(bundle.getString("GraficoJDialog.title")); // NOI18N
        setResizable(false);

        javax.swing.GroupLayout jPanelGraficoLayout = new javax.swing.GroupLayout(jPanelGrafico);
        jPanelGrafico.setLayout(jPanelGraficoLayout);
        jPanelGraficoLayout.setHorizontalGroup(
            jPanelGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelGraficoLayout.setVerticalGroup(
            jPanelGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 248, Short.MAX_VALUE)
        );

        jButtonVoltar.setText(bundle.getString("GraficoJDialog.jButtonVoltar.text")); // NOI18N
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });

        if(!Double.isNaN(snellController.getSnell().getAnguloRefratado())){
            jLabelRefracao.setText(java.util.ResourceBundle.getBundle("ui/Bundle").getString("Ângulo Refractado") +
                String.format(": %.2fº", snellController.getSnell().getAnguloRefratado()));
        }else{
            jLabelRefracao.setText("Ângulo Refratado: não existe");
        }

        jLabelNomeMeio1.setText(snellController.getSnell().getMeio1().getNome());

        jLabelIndiceMeio1.setText(String.format("(%.2f)", snellController.getSnell().getMeio1().getIndice()));

        jLabelIncidencia.setText(java.util.ResourceBundle.getBundle("ui/Bundle").getString("Ângulo Incidente") +
            String.format(": %.2fº", snellController.getSnell().getAnguloIncidencia()));

        jLabelNomeMeio2.setText(snellController.getSnell().getMeio2().getNome());

        jLabelIndiceMeio2.setText(String.format("(%.2f)", snellController.getSnell().getMeio2().getIndice()));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNomeMeio1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelIndiceMeio1))
                            .addComponent(jLabelIncidencia)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNomeMeio2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelIndiceMeio2))
                            .addComponent(jLabelRefracao))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanelGrafico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 462, Short.MAX_VALUE)
                        .addComponent(jButtonVoltar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeMeio1)
                            .addComponent(jLabelIndiceMeio1))
                        .addGap(18, 18, 18)
                        .addComponent(jLabelIncidencia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeMeio2)
                            .addComponent(jLabelIndiceMeio2))
                        .addGap(18, 18, 18)
                        .addComponent(jLabelRefracao)
                        .addGap(4, 4, 4))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelGrafico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jButtonVoltar)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GraficoJDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(() -> {
            GraficoJDialog dialog = new GraficoJDialog(new javax.swing.JFrame(), true, null, null);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JLabel jLabelIncidencia;
    private javax.swing.JLabel jLabelIndiceMeio1;
    private javax.swing.JLabel jLabelIndiceMeio2;
    private javax.swing.JLabel jLabelNomeMeio1;
    private javax.swing.JLabel jLabelNomeMeio2;
    private javax.swing.JLabel jLabelRefracao;
    private javax.swing.JPanel jPanelGrafico;
    // End of variables declaration//GEN-END:variables
}
