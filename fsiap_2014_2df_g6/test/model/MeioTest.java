/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class MeioTest {

    private final Meio testMeio;

    public MeioTest() {
        testMeio = new Meio("Agua", 2.25, 1.33);
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Teste class Meio");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Teste terminado");
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Meio.
     */
    @Test
    public void testGetNome() {
        System.out.println("Teste metodo getNome");
        String expResult = "Agua";
        String result = testMeio.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNome method, of class Meio.
     */
    @Test
    public void testSetNome() {
        System.out.println("Teste metodo setNome");
        String nome = "Ar";
        testMeio.setNome(nome);
        String result = testMeio.getNome();
        assertEquals(nome, result);
    }

    /**
     * Test of getVelocidade method, of class Meio.
     */
    @Test
    public void testGetVelocidade() {
        System.out.println("Teste metodo getVelocidade");
        double expResult = 2.25;
        double result = testMeio.getVelocidade();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setVelocidade method, of class Meio.
     */
    @Test
    public void testSetVelocidade() {
        System.out.println("Teste metodo setVelocidade");
        double velocidade = 3.0;
        testMeio.setVelocidade(velocidade);
        double result = testMeio.getVelocidade();
        assertEquals(velocidade, result, 0.0);
    }

    /**
     * Test of getIndice method, of class Meio.
     */
    @Test
    public void testGetIndice() {
        System.out.println("Teste metodo getIndice");
        double expResult = 1.33;
        double result = testMeio.getIndice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setIndice method, of class Meio.
     */
    @Test
    public void testSetIndice() {
        System.out.println("Teste metodo setIndice");
        double indice = 1.0;
        testMeio.setIndice(indice);
        double result = testMeio.getIndice();
        assertEquals(indice, result, 0.0);
    }

    /**
     * Test of toString method, of class Meio.
     */
    @Test
    public void testToString() {
        System.out.println("Teste metodo toString");
        String expResult = "Meio: Agua velocidade: 2,25m/s indice de refracao: 1,33";
        String result = testMeio.toString();
        assertEquals(expResult, result);
    }

}
