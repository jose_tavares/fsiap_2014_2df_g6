/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class SnellTest {

    private final Meio meio1;
    private final Meio meio2;
    private final Snell snell;

    public SnellTest() {
        meio1 = new Meio("Agua", 2.25, 1.33);
        meio2 = new Meio("Ar", 3.0, 1.0);
        snell = new Snell(meio1, meio2, 45.0);
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Teste class Snell");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Teste terminado");
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMeio1 method, of class Snell.
     */
    @Test
    public void testGetMeio1() {
        System.out.println("Teste metodo getMeio1");
        Meio expResult = meio1;
        Meio result = snell.getMeio1();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMeio1 method, of class Snell.
     */
    @Test
    public void testSetMeio1() {
        System.out.println("Teste metodo setMeio1");
        Meio expResult = meio2;
        snell.setMeio1(meio2);
        Meio result = snell.getMeio1();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMeio2 method, of class Snell.
     */
    @Test
    public void testGetMeio2() {
        System.out.println("Teste metodo getMeio2");
        Meio expResult = meio2;
        Meio result = snell.getMeio2();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMeio2 method, of class Snell.
     */
    @Test
    public void testSetMeio2() {
        System.out.println("Teste metodo setMeio2");
        Meio expResult = meio1;
        snell.setMeio2(meio1);
        Meio result = snell.getMeio2();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAnguloIncidencia method, of class Snell.
     */
    @Test
    public void testGetAnguloIncidencia() {
        System.out.println("Teste metodo getAnguloIncidencia");
        double expResult = 45.0;
        double result = snell.getAnguloIncidencia();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAnguloIncidencia method, of class Snell.
     */
    @Test
    public void testSetAnguloIncidencia() {
        System.out.println("Teste metodo setAnguloIncidencia");
        double incidencia = 48.0;
        snell.setAnguloIncidencia(incidencia);
        double result = snell.getAnguloIncidencia();
        assertEquals(incidencia, result, 0.0);
    }

    /**
     * Test of getAnguloRefratado method, of class Snell.
     */
    @Test
    public void testGetAnguloRefratadoByAnguloRefratadoMethod() {
        System.out.println("Teste metodo getAnguloRefratado usando metodo anguloRefratado");
        double expResult = Math.toDegrees(Math.asin(meio1.getIndice()
                * Math.sin(Math.toRadians(snell.getAnguloIncidencia())) / meio2.getIndice()));
        snell.anguloRefratado();
        double result = snell.getAnguloRefratado();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of anguloCritico method, of class Snell.
     */
    @Test
    public void testAnguloCritico() {
        System.out.println("Teste metodo anguloCritico");
        double expResult = Math.toDegrees(Math.asin(meio2.getIndice() / meio1.getIndice()));
        snell.anguloCritico();
        double result = snell.getAnguloIncidencia();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of toString method, of class Snell.
     */
    @Test
    public void testToString() {
        System.out.println("Teste metodo toString");
        String expResult = "Meio1:\n"
                + "Meio: Agua velocidade: 2,25m/s indice de refracao: 1,33\n"
                + "Meio2:\n"
                + "Meio: Ar velocidade: 3,00m/s indice de refracao: 1,00\n"
                + "Angulos:\n"
                + "incidencia - 45,00º refletido - 0,00º reflatado - 0,00º";
        String result = snell.toString();
        assertEquals(expResult, result);
    }

}
